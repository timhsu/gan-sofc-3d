import tensorflow as tf
import keras
import keras.backend as K

import numpy as np
import time
from pathlib  import Path

from utils.architectures import *
from utils.logging_funcs import *
from utils.cmdline_args  import *
from utils.misc          import *
from utils.sampling      import *


############ DATA/PARAMS DEFINITIONS #############################################
# parse data/params args from command line
args = parse_cmdline()

dataset_path   = args.dataset
dataset_name   = args.dataset_name
image_size     = args.image_size
num_filters_c  = args.num_filters_c
num_filters_g  = args.num_filters_g
num_samples    = args.num_samples
run_id         = args.run_id
log_path       = args.log_path

BATCH_SIZE     = args.batch_size
TRAINING_RATIO = args.train_ratio
LATENT_DIM     = args.latent_dim
EPOCHS         = args.epochs

LEARN_RATE_C   = args.lr_c
LEARN_RATE_G   = args.lr_g
BN_MOMENTUM    = args.bn_momentum
BETA_1         = args.beta_1
BETA_2         = args.beta_2

# add subdirectory with datetime+run_id info onto log_path
log_path = Path(log_path)/run_id


############ LOAD DATASET AND CHECK INFO ########################################
x_train = keras.utils.HDF5Matrix(dataset_path, dataset_name)
x_train = np.array(x_train)
print("3D microstructure shape:", x_train.shape)

image_size  = tuple(image_size)
print(f"Sampling {image_size}-shaped images from microstructure")

image_batch = sample_subimage_batch(x_train, image_size, BATCH_SIZE)
image_batch = image_batch/255*2 - 1
print("Sampled image batch min/max (normalized): {:.6f} {:.6f}".format(
    image_batch.min(), image_batch.max() ))


############ MODEL DEFINITIONS ##################################################
# construct generator and critric
initial_epoch, latest_c_fname, latest_g_fname = check_resume_session(log_path)
if initial_epoch > 1:
    critic = create_critic(image_size, num_filters_c)
    critic.load_weights(latest_c_fname)
    generator = keras.models.load_model(latest_g_fname)
else:
    critic    = create_critic(image_size, num_filters_c)
    generator = create_generator(image_size, num_filters_g,
                                LATENT_DIM, BN_MOMENTUM)
print("CRITIC ARCHITECTURE"); critic.summary()
print("GENERATOR ARCHITECTURE"); generator.summary()

save_model_architecture((critic, generator),
                        (num_filters_c, num_filters_g),
                         log_path/'txt')

# define optimizers
c_optimizer = keras.optimizers.Adam(LEARN_RATE_C, beta_1=BETA_1, beta_2=BETA_2)
g_optimizer = keras.optimizers.Adam(LEARN_RATE_G, beta_1=BETA_1, beta_2=BETA_2)

# construct model for training generator
critic.trainable    = False

z_for_generator     = Input(shape=(LATENT_DIM,))
generated_images    = generator(z_for_generator)
critic_output       = critic(generated_images)
model_for_generator = Model(z_for_generator, critic_output)

model_for_generator.compile(optimizer=g_optimizer, loss=wasserstein_loss)

# construct model for training critic
critic.trainable    = True
generator.trainable = False

real_images         = Input(shape=(*image_size, 1))
z_for_critic        = Input(shape=(LATENT_DIM,))
fake_images         = generator(z_for_critic)
critic_output_real  = critic(real_images)
critic_output_fake  = critic(fake_images)
model_for_critic    = Model([real_images,        z_for_critic],
                            [critic_output_real, critic_output_fake])

model_for_critic.compile(optimizer=c_optimizer, 
                         loss=[wasserstein_loss, wasserstein_loss])

# check estimated GPU memory usage lower bound
critic.trainable     = True
generator.trainable  = True
est_c_mem = get_model_memory_usage(BATCH_SIZE, critic)
est_g_mem = get_model_memory_usage(BATCH_SIZE, generator)

print(f"Estimated critic    memory usage (lower bound): {est_c_mem} Gbytes.")
print(f"Estimated generator memory usage (lower bound): {est_g_mem} Gbytes.")
print(f"Estimated combined (lower bound): {est_c_mem + est_g_mem} Gbytes.")


############ TRAINING ITERATIONS ################################################
steps_per_epoch = int(num_samples // (BATCH_SIZE*TRAINING_RATIO))

real_labels =   np.ones((BATCH_SIZE, 1))
fake_labels = - np.ones((BATCH_SIZE, 1))

np.random.seed(0)
test_noise = np.random.randn(BATCH_SIZE, LATENT_DIM)

for epoch in range(initial_epoch, initial_epoch + EPOCHS):
    print(f"EPOCH {epoch:4d}")

    C_loss = []
    G_loss = []

    progress_bar = keras.utils.Progbar(target=steps_per_epoch)
    
    start_time = time.time()
    for step in range(steps_per_epoch):
        # train critic
        for k in range(TRAINING_RATIO):
            image_batch = sample_subimage_batch(x_train, image_size, BATCH_SIZE)
            image_batch = image_batch/255*2 - 1
            noise       = np.random.randn(BATCH_SIZE, LATENT_DIM)

            critic.trainable    = True
            generator.trainable = False
            C_loss.append(model_for_critic.train_on_batch([image_batch, noise],
                                                          [real_labels, fake_labels]))

        # train generator
        critic.trainable    = False
        generator.trainable = True
        noise = np.random.randn(BATCH_SIZE, LATENT_DIM)
        G_loss.append(model_for_generator.train_on_batch(noise, real_labels))
        
        progress_bar.update(step+1)
    
    epoch_time = time.time() - start_time
    
    # log files and information
    save_img_batch(generator.predict(test_noise), epoch, log_path/'imgs')
    save_epoch_time(epoch_time,                   epoch, log_path/'csv')
    save_losses((C_loss, G_loss),                 epoch, log_path/'csv')
    save_loss_plots(                                     log_path/'figs')
    save_models(generator, critic,                epoch, log_path/'models')
