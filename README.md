# gan-sofc-3d
A TensorFlow/Keras implementation of spectral-normalized Wasserstein GAN written for 3D (solid oxide fuel cell) microstructure generation.

## Requirements
- TensorFlow and Keras
- Python standard library
- matplotlib
- CUDA libraries and NVIDIA drivers for GPU acceleration

Using `conda` may be the easiest method to install the required libraries:
```
conda install tensorflow-gpu keras matplotlib
```

Additional installation steps are required for running _distributed_ (using multiple GPUs across nodes) training sessions. The distributed training implementation is done by using [Horovod](https://github.com/horovod/horovod). You will probably need to ask your supercomputer admin to install Horovod (and its depedencies) for you.

## What does each branch do?
The __master__ branch provides baseline features that all other branches should have. If you are a beginner in general, you should start with this branch. You cannot run multi-GPU trainings on this branch.

The __keras-multi-gpu__ branch allows single-node, multi-GPU data parallelism. This is easily done by setting the `--keras_num_gpus` command-line argument. It is recommended that you get familiar with the __master__ branch before you use this branch.

The __horovod__ branch allows multi-node, multi-GPU (a.k.a. distributed training) data parallelism. This is best done using a job submission script on a supercomputer. It is recommended that you get familiar with the __keras-multi-gpu__ branch before you use this branch.

## Usage 
Run the `main.py` script with the appropriate command-line arguments. You are encouraged to write a bash script with the following example command:
```
python main.py \
    --dataset /path/to/dataset.h5 \
    --image_size 64x64x64 \
    --run_id my-first-training \
    --num_samples 100000 \
    --epochs 100 \
    --batch_size 64
```

__Tip__: If you are runnning the script from the terminal (as opposed to submitting a job on a cluster), you can redirect stderr output to a file to have a much cleaner console output. For example, you can run the following:
```
python main.py 2>log/stderr.txt \
    --dataset /path/to/dataset.h5 \
    --image_size 64x64x64 \
    --run_id my-first-training
```

If you want to run distributed training, check out Horovod's documentation on [running Horovod](https://github.com/horovod/horovod/blob/master/docs/running.rst).

Please read further in order to understand the command-line arguments.

### Required arguments
#### `--dataset`
Use `--dataset [path]` to indicate the microstructure dataset for the training. `path` should be the path to an HDF5 file that contains a 3D microstructure image saved in 8-bit unsigned integer format. Currently only the HDF5 format is considered.

##### Note on the dataset
Currently, the code only supports HDF5 as the data format. Whether you are sampling from a collection of 2D images or a single (preferably large) 3D image, the HDF5 data file should contain a 3-dimensional numpy array. In the 3D case, you can wrap your 3D image data like this:
```python
import h5py
import numpy as np

image_data = ... # Read your image data into a 3D numpy array
assert image_data.ndim == 3

hdf5_fname   = '/path/to/my_data.h5'
dataset_name = 'x_train'
with h5py.File(hdf5_fname, mode='w') as f:
    f.create_dataset(dataset_name, dataset_shape, dtype=np.uint8)
    f[dataset_name][...] = image_data
```

In the 2D case, you should wrap your data like this:
```python
import h5py
import numpy as np

image_data = ... # Read your image data into a list of 2D numpy arrays, or a 3D array (with the first axis being the "list" dimension)
assert image_data[0].ndim == 2

hdf5_fname   = '/path/to/my_data.h5'
dataset_name = 'x_train'
with h5py.File(hdf5_fname, mode='w') as f:
    f.create_dataset(dataset_name, dataset_shape, dtype=np.uint8)
    for i in range(len(image_data)):
        f[dataset_name][i, ...] = image_data[i]
```

#### `--image_size`
During training, the code samples 2D/3D subimages from the 3D microstructure and feeds them into the GAN model. The size (also called shape) of the 2D/3D subimages is determined by `--image_size [size]`. `size` should be a 'x'-delimited string. For example, to sample 64x64x64 (64 by 64 by 64 voxels) subvolumes, use `--image_size "64x64x64"`. You can also sample 2D subimages by using `--image_size "96x96"`. 

#### `--run_id`
Use `--run_id [id_str]` to set an identifier string (`id_str`) to be associated with the training session. `id_str` is also the sub-directory name for all log information and files. For example, `--run_id 2019-06-23-my-first-training` specifies the path `./log/2019-06-23-my-first-training/` for saving log files.

__Tip__: You can resume a previously terminated session by specifying the same `id_str`. The code should be smart enough to pick up the logged model weights and resume training. Of course, this new session will log files under the same directory. When resuming a previous session, you should make sure all other parameters are the same as before.

### Optional arguments
Optional arguments have default values. You can check them by running `python main.py -h`. Some of the more common ones that you might be setting frequently are explained below.

#### `--num_filters_c` and `--num_filters_g`
The GAN architecture can be adjusted by setting the number of convolutional filters at each critic/generator layer. Higher number of filters means higher complexity for the critic/generator. The input should be a '-'-delimited string. For example, you can specify the following:
```
--num_filters_c 32-64-128-256 \
--num_filters_g 256-128-64-32
```
The above code means the critic will have 32 conv. filters at the first layer (actually sub-layers), 64 conv. filters at the second layer, and so on. Similarly, the generator will have 256 conv. filters at the first layer, 128 conv. filters at the second layer, and so on.

The above code is good enough for 2D training, but 3D training is a lot more difficult and finicky, I recommend you to experiment around to find the best level of complexity.

To fully understand the meaning of these two command-line arguments, some knowledge in deep learning / computer vision / GAN theory is required. You may contact me for questions ("what is a convolutional filter?")

__Tip__: You can also change the number of layers. For example: `---num_filters_c 64-64-128-128-256` gives you a critic that consists of only 5 layers, while `---num_filters_g 256-128-128-64-64-32-32` gives you a generator that consists of 7 layers. In practice, the maximum number of layers is limited by the sampling image size, and I have observed that it usually should be 4 or 5 layers.

#### `--log_path`
During training, the code saves various log information and files under the parent directory specified by `--log_path [path]`. The default for this path is simply `./log/`.

#### `--num_samples`
`--num_samples [num]` specifies the number of samples for each epoch. This number does not have much physical meaning. But for debugging/testing purposes, it's convenient to set it to a low value (e.g, 1000), because the code saves various log information at the end of every epoch. To compare the performances of different training runs, you should make this number consistent across the different sessions.

#### `--epochs`
Use `--epochs [num]` to specify how many epochs you want to run for.

#### `--batch_size`
If you encounter GPU memory limit issue, use a smaller batch size with `--batch_size [num]`, where `num` should be a multiple of 8. The code prints estimated GPU memory usage right before training. So keep an eye on that, as well.

#### `--keras_num_gpus`
On the __keras-multi-gpu__ branch, you can specify the number of GPUs to use by using `--keras_num_gpus [num]`.

### Hyperparameters
Hyperparameters are model parameters to be constantly tweaked and tuned in order to find the best model "accuracy". If you run `python main.py -h` you can see how to specify them. An example is the learning rate for the critic network, specified by `--lr_c`. But normally if you're not actively involved in this project, then you don't have to bother understanding/setting them; the default values are good enough, I think.
