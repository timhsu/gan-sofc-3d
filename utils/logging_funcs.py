import matplotlib
matplotlib.use('Agg') # don't show plots on screen
import matplotlib.pyplot as plt

import numpy as np
import os
import csv
from scipy import signal
from pathlib import Path

################################################################################

def save_img_batch(img_batch, epoch, save_path):
    img_batch = np.squeeze(img_batch, axis=-1)
    n, *img_shape = img_batch.shape

    if n >= 25:
        ncol, nrow = 5, 5
    elif n >= 16:
        ncol, nrow = 4, 4
    elif n >= 9:
        ncol, nrow = 3, 3
    else:
        ncol, nrow = 2, 2

    fig = plt.figure(figsize=(10,10))
    for i in range(ncol*nrow):
        ax = fig.add_subplot(ncol, nrow, i+1)
        ax.axis('off')
        if len(img_shape) == 3:
            ax.imshow(img_batch[i, int(img_shape[0]/2), ...], cmap='gray')
        else:
            ax.imshow(img_batch[i, ...], cmap='gray')

    fig.suptitle(f"Epoch {epoch:4d}", size=20)
    fig.tight_layout()
    fig.subplots_adjust(top=0.92)

    if not os.path.exists(save_path): os.makedirs(save_path)
    fname = str(save_path/f"ep{epoch:04d}.png")
    fig.savefig(fname, dpi=150)
    plt.close(fig)

################################################################################

def save_epoch_time(epoch_time, epoch, save_path):    
    if not os.path.exists(save_path): os.makedirs(save_path)
    
    fname = str(save_path/'epoch_time.csv')
    file_exists = os.path.isfile(fname)
    
    header = ['epoch','time(s)']
    with open(fname, 'a') as f:
        writer = csv.DictWriter(f, fieldnames=header)
        if not file_exists:
            writer.writeheader()
        writer.writerow({'epoch':      epoch, 
                         'time(s)': f"{epoch_time:.2f}"})

################################################################################

def save_losses(epoch_losses, epoch, save_path):
    if not os.path.exists(save_path): os.makedirs(save_path)

    C_loss, G_loss = epoch_losses
    C_loss = np.array(C_loss)

    epochs_c = np.linspace(epoch-1, epoch, len(C_loss)+1)
    epochs_g = np.linspace(epoch-1, epoch, len(G_loss)+1)
    epochs_c = epochs_c[1:] 
    epochs_g = epochs_g[1:] 

    fname = str(save_path/'c_loss.csv')
    file_exists = os.path.isfile(fname)
    with open(fname, 'a') as f:
        writer = csv.writer(f)
        if not file_exists:
            writer.writerow(['epochs', 'c_loss_mean', 'c_loss_real', 'c_loss_fake'])
        rows  = zip(epochs_c, C_loss[:,0], C_loss[:,1], C_loss[:,2])
        for row in rows:
            writer.writerow(row)

    fname = str(save_path/'g_loss.csv')
    file_exists = os.path.isfile(fname)
    with open(fname, 'a') as f:
        writer = csv.writer(f)
        if not file_exists:
            writer.writerow(['epochs', 'g_loss'])
        rows  = zip(epochs_g, G_loss)
        for row in rows:
            writer.writerow(row)

################################################################################

def save_loss_plots(save_path):
    if not os.path.exists(save_path): os.makedirs(save_path)

    data_c = np.genfromtxt(save_path/'../csv/c_loss.csv', delimiter=',', skip_header=1)
    data_g = np.genfromtxt(save_path/'../csv/g_loss.csv', delimiter=',', skip_header=1)

    epochs_c = data_c[:, 0]
    epochs_g = data_g[:, 0]
    c_losses = data_c[:, 1:]
    g_loss   = data_g[:, 1:]

    plt.rc('font', size=12)
    plt.plot(epochs_c, c_losses[:,0], linewidth=0.5, label='c_loss')
    plt.plot(epochs_c, c_losses[:,1], linewidth=0.5, label='c_loss_real')
    plt.plot(epochs_c, c_losses[:,2], linewidth=0.5, label='c_loss_fake')
    plt.plot(epochs_g, g_loss, linewidth=0.5, label='g_loss')
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.legend()
    
    fname = str(save_path / 'losses.png')
    plt.tight_layout()
    plt.savefig(fname)
    plt.close()

    plt.rc('font', size=12)
    plt.plot(epochs_g, g_loss, linewidth=0.5, label='g_loss')
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.legend()
    
    fname = str(save_path / 'g_loss.png')
    plt.tight_layout()
    plt.savefig(fname)
    plt.close()

################################################################################

def save_models(generator, critic, epoch, save_path):
    if not os.path.exists(save_path): os.makedirs(save_path)
    
    fname_critic    = str(save_path/f"critic-weights-ep{epoch:03d}.h5")
    fname_generator = str(save_path/f"generator-ep{epoch:03d}.h5")
    critic.save_weights(fname_critic)
    generator.save(fname_generator)

################################################################################

def save_model_architecture(models, num_filters, save_path):
    if not os.path.exists(save_path): os.makedirs(save_path)

    critic, generator = models
    num_filters_c, num_filters_g = num_filters

    fname = str(save_path/'architecture.txt')
    with open(fname, 'w') as f:
        f.write("CRITIC ARCHITECTURE\n")
        print(num_filters_c, file=f)
        critic.summary(print_fn=lambda x: f.write(x + '\n'))
        f.write("GENERATOR ARCHITECTURE\n")
        print(num_filters_g, file=f)
        generator.summary(print_fn=lambda x: f.write(x + '\n'))