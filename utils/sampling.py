import numpy as np

################################################################################

def sample_2d_subimage(im, subimage_size, apply_symmetry=True):
    """
    Sample a 2D subimage

    Parameters
    ----------
    im : ndarray
        A 3D image data stored as a numpy ndarray.
    subimage_size : tuple of ints
        Size of the subimage to be sampled.
    apply_symmetry : bool
        If set to True, each subimage will be randomy rotated and flipped.

    Returns
    -------
    subimage : ndarray
        The sampled 2D subimage.
    """
    
    # if lock_axis == 0:
    #     idx_lock = np.random.randint(im.shape[0])
    #     im_2d = im[idx_lock, :, :]
    # if lock_axis == 1:
    #     idx_lock = np.random.randint(im.shape[1])
    #     im_2d = im[:, idx_lock, :]
    # if lock_axis == 2:
    #     idx_lock = np.random.randint(im.shape[2])
    #     im_2d = im[:, :, idx_lock]

    idx_lock = np.random.randint(im.shape[0])
    im_2d = im[idx_lock, :, :]

    idx_0 = np.random.randint(im_2d.shape[0] - (subimage_size[0]-1))
    idx_1 = np.random.randint(im_2d.shape[1] - (subimage_size[1]-1))

    subimage = im_2d[idx_0:(idx_0+subimage_size[0]),
                     idx_1:(idx_1+subimage_size[1])]

    if apply_symmetry:
        subimage = np.rot90(subimage, k=np.random.randint(4))
        if np.random.choice([True, False]): subimage = np.fliplr(subimage)
        if np.random.choice([True, False]): subimage = np.flipud(subimage)

    return subimage
    
################################################################################

def sample_3d_subimage(im, subimage_size, apply_symmetry=True):
    """
    Sample a 3D subimage

    Parameters
    ----------
    im : ndarray
        A 3D image data stored as a numpy ndarray.
    subimage_size : tuple of ints
        Size of the subvolume to be sampled.
    apply_symmetry : bool
        If set to True, each subimage will be randomy rotated and flipped.

    Returns
    -------
    subimage : ndarray
        The sampled 3D subimage.
    """
    
    im_dims       = np.array(im.shape)
    subimage_size = np.array(subimage_size)
    idx_max       = im_dims - (subimage_size-1)
    idx           = [np.random.randint(i) for i in idx_max]

    subimage = im[idx[0]:(idx[0]+subimage_size[0]),
                  idx[1]:(idx[1]+subimage_size[1]),
                  idx[2]:(idx[2]+subimage_size[2])]   

    if apply_symmetry:
        subimage = np.rot90(subimage, k=np.random.randint(4), axes=(1,2))
        # subimage = np.rot90(subimage, k=np.random.randint(4), axes=(0,1))
        # subimage = np.rot90(subimage, k=np.random.randint(4), axes=(0,2))
        subimage = np.rot90(subimage, k=np.random.choice([0,2]), axes=(0,1))
        subimage = np.rot90(subimage, k=np.random.choice([0,2]), axes=(0,2))
        
        if np.random.choice([True, False]): subimage = np.flip(subimage, axis=0)
        if np.random.choice([True, False]): subimage = np.flip(subimage, axis=1)
        if np.random.choice([True, False]): subimage = np.flip(subimage, axis=2)

    return subimage

################################################################################

def sample_subimage_batch(im, subimage_size, batch_size, apply_symmetry=True):
    """
    Sample a batch of 2D/3D subimages

    Parameters
    ----------
    im : ndarray
        An image data stored as a numpy ndarray.
    subimage_size : tuple of ints
        Size of the subvolume to be sampled.
    batch_size : int
        Batch size (number of subimages to be sampled.)
    apply_symmetry : bool
        If set to True, each subimage will be randomy rotated and flipped.

    Returns
    -------
    subimage batch : ndarray
        The sampled subimage batch.
    """

    subimage_batch = np.zeros((batch_size, *subimage_size, 1), dtype=np.uint8)

    if len(subimage_size) == 3:
        sample_subimage = sample_3d_subimage
    elif len(subimage_size) == 2:
        sample_subimage = sample_2d_subimage
    else:
        raise ValueError("Your image size is neither 2D nor 3D!")

    for i in range(batch_size):
        subimage = sample_subimage(im, subimage_size, apply_symmetry)
        subimage_batch[i, ..., 0] = subimage

    return subimage_batch