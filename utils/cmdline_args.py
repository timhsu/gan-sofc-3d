import argparse

def parse_cmdline():
    parser = argparse.ArgumentParser(description="GAN for 3D microstructure generation")

    parser.add_argument(
            '-d',
            '--dataset',
            required=True,
            type=str,
            help="Path to the dataset file in HDF5 format.",
    )

    parser.add_argument(
            '-is',
            '--image_size',
            required=True,
            type=lambda s: [int(item) for item in s.split('x')],
            help="Image size to sample from the 3D microstructure (example: '64x64x64')."
    )

    parser.add_argument(
            '-id',
            '--run_id',
            required=True,
            type=str,
            help="Identifier string for this training session."
    )

    parser.add_argument(
            '-fc',
            '--num_filters_c',
            required=False,
            type=lambda s: [int(item) for item in s.split('-')],
            default='32-64-128-256',
            help="Number of filters for each layer of Critic (default: ''32-64-128-256).",
            metavar=''
    )

    parser.add_argument(
            '-fg',
            '--num_filters_g',
            required=False,
            type=lambda s: [int(item) for item in s.split('-')],
            default='256-128-64-32',
            help="Number of filters for each layer of Generator (default: '256-128-64-32').",
            metavar=''
    )

    parser.add_argument(
            '-dn',
            '--dataset_name',
            required=False,
            type=str,
            default='x_train',
            help="Dataset name stored in the HDF5 file (default: 'x_train').",
            metavar=''
    )

    parser.add_argument(
            '-ns',
            '--num_samples',
            required=False,
            type=int,
            default=100000,
            help="Number of samples per epoch (default: 100000).",
            metavar=''
    )

    parser.add_argument(
            '-lp',
            '--log_path',
            required=False,
            type=str,
            default='./log/',
            help="Parent directory for log files (default: ./log/).",
            metavar=''
    )

    parser.add_argument(
            '-B',
            '--batch_size',
            required=False,
            type=int,
            default=64,
            help="Batch size per node (default: 64).",
            metavar=''
    )

    parser.add_argument(
            '-R',
            '--train_ratio',
            required=False,
            type=int,
            default=5,
            help="Number of updates for critic before the generator update (default: 5).",
            metavar=''
    )

    parser.add_argument(
            '-L',
            '--latent_dim',
            required=False,
            type=int,
            default=100,
            help="Dimension of the latent/noise vector for the generator (default: 100).",
            metavar=''
    )

    parser.add_argument(
            '-E',
            '--epochs',
            required=False,
            type=int,
            default=100,
            help="Number of epochs to run (default: 100).",
            metavar=''
    )

    parser.add_argument(
            '-C',
            '--lr_c',
            required=False,
            type=float,
            default=0.0002,
            help="Learning rate for critic (default: 0.0002).",
            metavar=''
    )

    parser.add_argument(
            '-G',
            '--lr_g',
            required=False,
            type=float,
            default=0.0002,
            help="Learning rate for generator (default: 0.0002).",
            metavar=''
    )

    parser.add_argument(
            '-M',
            '--bn_momentum',
            required=False,
            type=float,
            default=0.99,
            help="BatchNorm momentum parameter (default: 0.99).",
            metavar=''
    )

    parser.add_argument(
            '-1',
            '--beta_1',
            required=False,
            type=float,
            default=0.1,
            help="Adam optimizer beta1 parameter (default: 0.1).",
            metavar=''
    )

    parser.add_argument(
            '-2',
            '--beta_2',
            required=False,
            type=float,
            default=0.9,
            help="Adam optimizer beta2 parameter (default: 0.9).",
            metavar=''
    )
    
    args = parser.parse_args()

    return args
