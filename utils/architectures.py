import keras.backend as K
from keras.layers import *
from keras.models import Model

import numpy as np

from utils.SpectralNormalizationKeras import DenseSN, ConvSN3D, ConvSN2D

################################################################################

def create_generator(img_shape, num_filters, latent_dim, bn_momentum):
    start_shape = np.array(img_shape) / (2**(len(num_filters)-1) )
    start_shape = tuple(start_shape.astype(int))

    if len(img_shape) == 3:
        Conv = Conv3D
        UpSampling = UpSampling3D
    else:
        Conv = Conv2D
        UpSampling = UpSampling2D

    #-------------------------------------------------------
    inputs = Input(shape=(latent_dim,))
    x = Dense(np.prod(start_shape) * num_filters[0])(inputs)
    x = Reshape((*start_shape, num_filters[0]))(x)
    
    for f in num_filters[1:]:
        x = UpSampling()(x)
        x = Conv(f, 4, padding="same", activation='relu')(x)
        x = BatchNormalization(momentum=bn_momentum)(x)
    x = Conv(1, 3, padding='same')(x)

    outputs = Activation('tanh')(x)

    return Model(inputs, outputs)

################################################################################

def create_critic(img_shape, num_filters):
    if len(img_shape) == 3:
        ConvSN = ConvSN3D
    else:
        ConvSN = ConvSN2D

    # -------------------------------------------------------
    inputs = Input(shape=(*img_shape, 1))
    x = inputs

    for f in num_filters[:-1]:
        x = ConvSN( f, 3, strides=1, padding="same")(x)
        x = LeakyReLU(0.1)(x)
        x = ConvSN( f, 4, strides=2, padding="same")(x)
        x = LeakyReLU(0.1)(x)

    x = ConvSN(num_filters[-1], 3, strides=1, padding="same")(x)
    x = LeakyReLU(0.1)(x)

    x = Flatten()(x)
    outputs = DenseSN(1)(x)

    return Model(inputs, outputs)

################################################################################

def wasserstein_loss(y_true, y_pred):
    return K.mean(y_true*y_pred)
