import keras.backend as K
import numpy as np
import os
import re
from glob import glob

def get_model_memory_usage(batch_size, model):
    shapes_mem_count = 0
    for l in model.layers:
        single_layer_mem = 1
        for s in l.output_shape:
            if s is None:
                continue
            single_layer_mem *= s
        shapes_mem_count += single_layer_mem

    trainable_count = np.sum([K.count_params(p) for p in set(model.trainable_weights)])
    non_trainable_count = np.sum([K.count_params(p) for p in set(model.non_trainable_weights)])

    number_size = 4.0
    if K.floatx() == 'float16':
         number_size = 2.0
    if K.floatx() == 'float64':
         number_size = 8.0

    total_memory = number_size*(batch_size*shapes_mem_count + trainable_count + non_trainable_count)
    gbytes = np.round(total_memory / (1024.0 ** 3), 3)
    return gbytes

################################################################################

def check_resume_session(log_path):
    models_path = log_path/'models'
    if os.path.exists(models_path):
        latest_c_fname = sorted(glob(str(models_path/'critic*.h5')))[-1]
        latest_g_fname = sorted(glob(str(models_path/'generator*.h5')))[-1]
        latest_c_epoch = int(re.search('0*(\d+).h5', latest_c_fname)[1])
        latest_g_epoch = int(re.search('0*(\d+).h5', latest_g_fname)[1])

        if latest_c_epoch == latest_g_epoch:
            initial_epoch = latest_c_epoch + 1
            print("Found model files that can be used to resume training")
            print("Critic file path:", latest_c_fname)
            print("Generator file path:", latest_g_fname)
            print("This training session will start at epoch:", initial_epoch)
            return initial_epoch, latest_c_fname, latest_g_fname
        else:
            print("Latest critic and generator model files do not have the same epoch number")
            return 1, None, None
    else:
        return 1, None, None
